//
//  ViewController.swift
//  MainAPp
//
//  Created by Aravind on 01/05/24.
//

import UIKit
import FrameworkTask

class ViewController: UIViewController {
    
    @IBOutlet weak var recipesTableView: UITableView!
    var recipesList: [recipes] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        RecipeClass.shared.getRecipesData { result in
            self.recipesList = result.recipes
            DispatchQueue.main.async {
                self.recipesTableView.reloadData()
            }
        }
        
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecipesCell", for: indexPath) as! RecipesCell
        cell.recipeName.text = recipesList[indexPath.row].name
        cell.recipeRating.text = String(recipesList[indexPath.row].rating)
        if let imageURL =  URL(string: recipesList[indexPath.row].image) {
            cell.recipeImage.imageFrom(url: imageURL)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    
}

class RecipesCell: UITableViewCell {
    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var recipeRating: UILabel!
    @IBOutlet weak var recipeType: UILabel!
    @IBOutlet weak var recipeName: UILabel!
}


extension UIImageView{
  func imageFrom(url:URL){
    DispatchQueue.global().async { [weak self] in
      if let data = try? Data(contentsOf: url){
        if let image = UIImage(data:data){
          DispatchQueue.main.async{
            self?.image = image
          }
        }
      }
    }
  }
}
